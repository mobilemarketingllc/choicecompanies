<?php
global $post;
$flooringtype = $post->post_type; 
$brand = get_field('brand') ;
$image = get_field('swatch_image_link') ? get_field('swatch_image_link'):"http://placehold.it/168x123?text=No+Image"; 
	
	if(strpos($image , 's7.shawimg.com') !== false){
		if(strpos($image , 'http') === false){ 
		$image = "http://" . $image;
	}	
		$image = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[300x300]&sink";
	}else{
		if(strpos($image , 'http') === false){ 
		$image = "https://" . $image;
	}	
		$image= "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[600x400]&sink";
	}

	$room_image_small = array();
	if(get_field('gallery_room_images')){
		$gallery_images = get_field('gallery_room_images');
		$gallery_img = explode("|",$gallery_images);
		
		foreach($gallery_img as  $key=>$value) {
			$room_image = $value;
		
			if(strpos($room_image , 's7.shawimg.com') !== false){
				if(strpos($room_image , 'http') === false){ 
					$room_image = "http://" . $room_image;
				}
				$room_image_small[] = $room_image ;
				$room_image = $room_image ;
			} else{
				if(strpos($room_image , 'http') === false){ 
					$room_image = "https://" . $room_image;
				}
				$room_image_small[]= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[150]&sink";
				$room_image= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[600x400]&sink";
			}
		}
	}
?>

<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/Product">
	<div class="fl-post-content clearfix " itemprop="text">
		<?php get_template_part('includes/product-color-slider');   ?>
		<div class="productInfo row productDesktop">
			<div class="container">
				<div class="colorTxt">Color</div>
				<!--			 <h2 class="colorName"><?php echo get_field('style');?></h2> -->
				<h2 class="colorName"><?php echo get_field('color');?></h2>
				<div class="col-md-3 col-sm-3">
					<div class="brandLogo">
						<?php

						if (get_field('brand') == 'Karastan'){ ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_karastan.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'SmartSolutions') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'STAINMASTER') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/stainmaster_logo.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'Armstrong') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/armstronglogo.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'Quick-Step') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/quickstep-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Daltile') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/daltile-logo.gif" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Floorscapes') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/floorscapes.jpg" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'OpenLine') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'ColorCenter') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-color-center.jpg" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Mohawk') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'NFA') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Hallmark Floors') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Shaw') { ?>
						<img src="https://www.choicefloorcenter.com/wp-content/uploads/2018/12/shaw_logo_k.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Bruce') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bruce-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Mannington') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mannington-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'IVC') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ivcus-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Congoleum') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/congoleum-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Aladdin Commercial') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/aladdin-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'California Classics') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/californiaClassics.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Hallmark') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hallmark.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Reward') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/reward.jpg" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Monarch') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/monarch.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Republic') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/republic.jpg" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Emser') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_emserTile.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Marazzi') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_marazzi.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'Shaw Floors') { ?>
						<img src="https://www.choicefloorcenter.com/wp-content/uploads/2018/12/shaw_logo_k.png" class="product-logo" class="img-responive"/>

						<?php } else { echo get_field('brand'); }?>


					</div>
					<div class="requestQuote">
						
						<a href="/flooring-coupon/?keyword=<?php echo $_GET['keyword']; ?>&brand=<?php echo $brand;?>">GET COUPON</a>        
					</div>

					<div class="scheduleEstimate">
						<a href="/schedule-an-estimate/">SCHEDULE AN ESTIMATE</a>        
					</div>

					<div class="scheduleEstimate">
						<a href="/flooring-financing/">GET FINANCING</a>        
					</div>

				</div>
				<div class="col-md-6 col-sm-6">
					<div class="img-responsive toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: cover;background-position:bottom">
						<img src="<?php echo $image; ?>" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" />
					</div>

					<?php if(get_field('gallery_room_images')){ ?>
					<div class="toggle-image-thumbnails">
					<a href="#" data-background="<?php echo $image ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $image ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a>
					<?php
					// var_dump($room_image_small);die();
						foreach($room_image_small  as $k=>$v){
					?>
					        <a href="#" data-background="<?php echo $v ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $v ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a>
					<?php
						}
					?>
					</div>
					<?php } ?>
				</div>
				<div class="col-md-3 col-sm-3">
					<?php get_template_part('includes/product-attributes'); ?>
				</div>

			</div>
		</div>

		<div class="productInfo row productMobile">
			<div class="colorTxt">Color</div>
			<h2 class="colorName"><?php echo get_field('color');?></h2>
			<div class="productImage">
				<div class="productImg" style="position:relative;">


					<div class="img-responsive toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: cover;background-position:bottom">
						<img src="<?php echo $image; ?>" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" />
					</div>

					<?php if(get_field('gallery_room_images')){ ?>
					<div class="toggle-image-thumbnails">
					<a href="#" data-background="<?php echo $image ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $image ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a>
						<?php
						// var_dump($room_image_small);die();
	foreach($room_image_small  as $k=>$v){
						?><a href="#" data-background="<?php echo $v ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $v ?>');background-size: cover; margin-top:10px;" title="<?php the_title_attribute(); ?>"></a><?php
	}
						?>
					</div>
					<?php } ?>

				</div>
				<div class="col-md-3 col-sm-3">
					<div class="brandLogo" style="text-align:center;">
						<?php

						if (get_field('brand') == 'Karastan'){ ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_karastan.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'SmartSolutions') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'STAINMASTER') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/stainmaster_logo.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'Armstrong') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/armstronglogo.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'Quick-Step') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/quickstep-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Daltile') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/daltile-logo.gif" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Floorscapes') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/floorscapes.jpg" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'OpenLine') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'ColorCenter') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-color-center.jpg" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Mohawk') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'NFA') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Hallmark Floors') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Shaw') { ?>
						<img src="https://www.choicefloorcenter.com/wp-content/uploads/2018/12/shaw_logo_k.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Bruce') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bruce-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Mannington') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mannington-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'IVC') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ivcus-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Congoleum') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/congoleum-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Aladdin Commercial') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/aladdin-logo.png" class="product-logo" class="img-responive"/>

						<?php }elseif (get_field('brand') == 'Shaw Floors') { ?>
						<img src="https://www.choicefloorcenter.com/wp-content/uploads/2018/12/shaw_logo_k.png" class="product-logo" class="img-responive"/>

						<?php } else { echo get_field('brand'); }?>


					</div>
					<div class="requestQuote">
						<a href="/request-quote/">REQUEST A QUOTE</a>        
					</div>

					<div class="scheduleEstimate">
						<a href="/schedule-an-estimate/">SCHEDULE AN ESTIMATE</a>        
					</div>

					<div class="scheduleEstimate">
						<a href="/flooring-coupon/?keyword=<?php echo $_GET['keyword']; ?>&brand=<?php echo $brand;?>">GET COUPON</a>    

					</div>

				</div>

				<div class="col-md-3 col-sm-3">
					<div style="height:50px; clear:both;">&nbsp;</div>
					<?php get_template_part('includes/product-attributes'); ?>      
				</div>


			</div>
			<?php //get_template_part('includes/product-attributes'); ?>
			<?php //get_template_part('includes/product-extra-info');   ?>
		</div>
		</article>

	<script>
		jQuery( ".swatch" ).click(function() {

			jQuery( "#roomimg" ).hide();
			jQuery( ".room i" ).hide();
			jQuery( ".swatch i" ).show();
			jQuery( "#swathcimg" ).show();
			jQuery(this).addClass( "Imgactive" );
			jQuery( ".room" ).removeClass("Imgactive");

		});
		jQuery( ".room" ).click(function() {

			jQuery( "#swathcimg" ).hide();
			jQuery( ".swatch i" ).hide();
			jQuery( ".room i" ).show();
			jQuery( "#roomimg" ).show();
			jQuery(this).addClass( "Imgactive" );
			jQuery( ".swatch" ).removeClass("Imgactive");
		});

		/* Mobile Js */
		jQuery( ".swatchM" ).click(function() {

			jQuery( "#roomimgM" ).hide();
			jQuery( ".roomM i" ).hide();
			jQuery( ".swatchM i" ).show();
			jQuery( "#swathcimgM" ).show();
			jQuery(this).addClass( "ImgactiveM" );
			jQuery( ".roomM" ).removeClass("ImgactiveM");

		});
		jQuery( ".roomM" ).click(function() {

			jQuery( "#swathcimgM" ).hide();
			jQuery( ".swatchM i" ).hide();
			jQuery( ".roomM i" ).show();
			jQuery( "#roomimgM" ).show();
			jQuery(this).addClass( "ImgactiveM" );
			jQuery( ".swatchM" ).removeClass("ImgactiveM");
		});

		jQuery(".viewmore").toggle(function(){
			jQuery(this).text("- view less").siblings(".showmore").show();    
		}, function(){
			jQuery(this).text("+ view more").siblings(".showmore").hide();    
		});
	</script>
