<?php
//$show_thumbs = FLTheme::get_setting('fl-posts-show-thumbs');
$brand = get_field('brand') ;
if( get_field('manufacturer') == 'Shaw' || get_field('manufacturer') == NULL){

	$a=array();

	$item = get_field('sku');
	$itemImage = explode("_", $item);	
	$imageNew= $itemImage[1] .'_'. $itemImage[0];


	//Code to check the image exits.	
	$url ='https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_MAIN?req=exists,text';
	$response = wp_remote_post($url, array('method' => 'GET'));		
	$ImageExist = $response["body"];	
	$exits = strpos($ImageExist,"catalogRecord.exists=0");		
	if ($exits === false) {	
		$a[]='https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_MAIN?qlt=60&wid=600&hei=400&fit=crop,0';
		$image = 'https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_MAIN?qlt=60&wid=600&hei=400&fit=crop,0';
		if(!is_singular( 'carpeting' )) {
			$url ='https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_ROOM?req=exists,text';
			$response = wp_remote_post($url, array('method' => 'GET'));		
			$ImageExist = $response["body"];	
			$exits = strpos($ImageExist,"catalogRecord.exists=0");		
			if ($exits === false) {	
				$a[]=get_field('room_scene_image_link') . "?qlt=60&wid=600&hei=400&fit=crop,0";
			}

		}
	} else {
		$a[]='https://shawfloors.scene7.com/is/image/ShawIndustriesRender/'.$imageNew.'_MAIN?qlt=60&wid=600&hei=400&fit=crop,0';
		$image = 'https://shawfloors.scene7.com/is/image/ShawIndustriesRender/'.$imageNew.'_MAIN?qlt=60&wid=600&hei=400&fit=crop,0';
	}




	//Code to check the image exits.	
	$url ='https://shawfloors.scene7.com/is/image/ShawIndustriesRender/'.$imageNew.'_MAIN?req=exists,text';
	$response = wp_remote_post($url, array('method' => 'GET'));		
	$ImageExist = $response["body"];	
	$exits = strpos($ImageExist,"catalogRecord.exists=0");		
	if ($exits === false) {	

		$item =  array("20130107_KITCHEN_1","20140116_DININGROOM_2_SQUARE","20130408_BEDROOM_2_SQUARE","20130122_DININGROOM_1","20141218_BEDROOM_1_SQUARE");
		if(is_singular( 'carpeting' )){
			$item[]= "20150504_STAIR_RUNNER_SQUARE";
		}

		foreach ($item as $img) {
			$a[]= '//shawfloors.scene7.com/is/image/ShawIndustries/?src=ir(ShawIndustriesRender/'.$img.'?res=20&src=is(ShawIndustriesRender/'.$imageNew.'_MAIN))?qlt=60&wid=600&hei=400&fit=crop,0';
		}
	}
}else{	
	if(get_field('swatch_image_link') == ""){
		$https= "https://placehold.it/600x400/F2F2F2/000000?text=Coming+Soon";
	}
	else if(strpos(get_field('swatch_image_link'), "http") === false){
		$https= "https:";
	} else {
		$https = "";
	}
	$image = $a[] = 'https://mobilem.liquifire.com/mobilem?source=url[' . $https . get_field('swatch_image_link') . ']&scale=size[600x400]&sink';

	if(get_field('room_scene_image_link') == ""){
		$https= "https://placehold.it/600x400/F2F2F2/000000?text=Coming+Soon";
	}
	else if(strpos(get_field('room_scene_image_link'), "http") === false){
		$https= "https:";
	} else {
		$https = "";
	}
	$a[]=  'https://mobilem.liquifire.com/mobilem?source=url['. $https .get_field('room_scene_image_link') . ']&scale=size[600x400]&sink';

}
?>
<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/Product">
	<div class="fl-post-content clearfix " itemprop="text">
		<?php get_template_part('includes/product-color-slider');   ?>
		<div class="productInfo row productDesktop">
			<div class="container">
				<div class="colorTxt">Color</div>
				<!--			 <h2 class="colorName"><?php echo get_field('style');?></h2> -->
				<h2 class="colorName"><?php echo get_field('color');?></h2>
				<div class="col-md-3 col-sm-3">
					<div class="brandLogo">
						<?php

						if (get_field('brand') == 'Karastan'){ ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_karastan.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'SmartSolutions') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'STAINMASTER') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/stainmaster_logo.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'Armstrong') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/armstronglogo.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'Quick-Step') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/quickstep-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Daltile') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/daltile-logo.gif" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Floorscapes') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/floorscapes.jpg" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'OpenLine') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'ColorCenter') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-color-center.jpg" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Mohawk') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'NFA') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Hallmark Floors') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Shaw') { ?>
						<img src="https://www.choicefloorcenter.com/wp-content/uploads/2018/12/shaw_logo_k.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Bruce') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bruce-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Mannington') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mannington-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'IVC') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ivcus-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Congoleum') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/congoleum-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Aladdin Commercial') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/aladdin-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'California Classics') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/californiaClassics.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Hallmark') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hallmark.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Reward') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/reward.jpg" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Monarch') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/monarch.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Republic') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/republic.jpg" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Emser') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_emserTile.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Marazzi') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_marazzi.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'Shaw Floors') { ?>
						<img src="https://www.choicefloorcenter.com/wp-content/uploads/2018/12/shaw_logo_k.png" class="product-logo" class="img-responive"/>

						<?php } else { echo get_field('brand'); }?>


					</div>
					<div class="requestQuote">
						
						<a href="/flooring-coupon/?keyword=<?php echo $_GET['keyword']; ?>&brand=<?php echo $brand;?>">GET COUPON</a>        
					</div>

					<div class="scheduleEstimate">
						<a href="/schedule-an-estimate/">SCHEDULE AN ESTIMATE</a>        
					</div>

					<div class="scheduleEstimate">
						<a href="/flooring-financing/">GET FINANCING</a>        
					</div>

				</div>
				<div class="col-md-6 col-sm-6">
					<div class="img-responsive toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: cover;background-position:bottom">
						<img src="<?php echo $image; ?>" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" />
					</div>

					<?php if(get_field('room_scene_image_link')){ ?>
					<div class="toggle-image-thumbnails">
						<?php

	foreach($a as $k=>$v){
						?><a href="#" data-background="<?php echo $v ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $v ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a><?php
	}
						?>
					</div>
					<?php } ?>





				</div>
				<div class="col-md-3 col-sm-3">
					<?php get_template_part('includes/product-attributes'); ?>
				</div>

			</div>
		</div>

		<div class="productInfo row productMobile">
			<div class="colorTxt">Color</div>
			<h2 class="colorName"><?php echo get_field('color');?></h2>
			<div class="productImage">
				<div class="productImg" style="position:relative;">


					<div class="img-responsive toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: cover;background-position:bottom">
						<img src="<?php echo $image; ?>" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" />
					</div>

					<?php if(get_field('room_scene_image_link')){ ?>
					<div class="toggle-image-thumbnails">
						<?php
	foreach($a as $k=>$v){
						?><a href="#" data-background="<?php echo $v ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $v ?>');background-size: cover; margin-top:10px;" title="<?php the_title_attribute(); ?>"></a><?php
	}
						?>
					</div>
					<?php } ?>

				</div>
				<div class="col-md-3 col-sm-3">
					<div class="brandLogo" style="text-align:center;">
						<?php

						if (get_field('brand') == 'Karastan'){ ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_karastan.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'SmartSolutions') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'STAINMASTER') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/stainmaster_logo.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'Armstrong') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/armstronglogo.png" class="product-logo" class="img-responive"/>

						<?php } elseif (get_field('brand') == 'Quick-Step') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/quickstep-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Daltile') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/daltile-logo.gif" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Floorscapes') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/floorscapes.jpg" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'OpenLine') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'ColorCenter') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-color-center.jpg" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Mohawk') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'NFA') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Hallmark Floors') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Shaw') { ?>
						<img src="https://www.choicefloorcenter.com/wp-content/uploads/2018/12/shaw_logo_k.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Bruce') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bruce-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Mannington') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mannington-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'IVC') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ivcus-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Congoleum') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/congoleum-logo.png" class="product-logo" class="img-responive"/>
						<?php } elseif (get_field('brand') == 'Aladdin Commercial') { ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/aladdin-logo.png" class="product-logo" class="img-responive"/>

						<?php }elseif (get_field('brand') == 'Shaw Floors') { ?>
						<img src="https://www.choicefloorcenter.com/wp-content/uploads/2018/12/shaw_logo_k.png" class="product-logo" class="img-responive"/>

						<?php } else { echo get_field('brand'); }?>


					</div>
					<div class="requestQuote">
						<a href="/request-quote/">REQUEST A QUOTE</a>        
					</div>

					<div class="scheduleEstimate">
						<a href="/schedule-an-estimate/">SCHEDULE AN ESTIMATE</a>        
					</div>

					<div class="scheduleEstimate">
						<a href="/flooring-coupon/?keyword=<?php echo $_GET['keyword']; ?>&brand=<?php echo $brand;?>">GET COUPON</a>    

					</div>

				</div>

				<div class="col-md-3 col-sm-3">
					<div style="height:50px; clear:both;">&nbsp;</div>
					<?php get_template_part('includes/product-attributes'); ?>      
				</div>


			</div>
			<?php //get_template_part('includes/product-attributes'); ?>
			<?php //get_template_part('includes/product-extra-info');   ?>
		</div>
		</article>

	<script>
		jQuery( ".swatch" ).click(function() {

			jQuery( "#roomimg" ).hide();
			jQuery( ".room i" ).hide();
			jQuery( ".swatch i" ).show();
			jQuery( "#swathcimg" ).show();
			jQuery(this).addClass( "Imgactive" );
			jQuery( ".room" ).removeClass("Imgactive");

		});
		jQuery( ".room" ).click(function() {

			jQuery( "#swathcimg" ).hide();
			jQuery( ".swatch i" ).hide();
			jQuery( ".room i" ).show();
			jQuery( "#roomimg" ).show();
			jQuery(this).addClass( "Imgactive" );
			jQuery( ".swatch" ).removeClass("Imgactive");
		});

		/* Mobile Js */
		jQuery( ".swatchM" ).click(function() {

			jQuery( "#roomimgM" ).hide();
			jQuery( ".roomM i" ).hide();
			jQuery( ".swatchM i" ).show();
			jQuery( "#swathcimgM" ).show();
			jQuery(this).addClass( "ImgactiveM" );
			jQuery( ".roomM" ).removeClass("ImgactiveM");

		});
		jQuery( ".roomM" ).click(function() {

			jQuery( "#swathcimgM" ).hide();
			jQuery( ".swatchM i" ).hide();
			jQuery( ".roomM i" ).show();
			jQuery( "#roomimgM" ).show();
			jQuery(this).addClass( "ImgactiveM" );
			jQuery( ".swatchM" ).removeClass("ImgactiveM");
		});

		jQuery(".viewmore").toggle(function(){
			jQuery(this).text("- view less").siblings(".showmore").show();    
		}, function(){
			jQuery(this).text("+ view more").siblings(".showmore").hide();    
		});
	</script>
