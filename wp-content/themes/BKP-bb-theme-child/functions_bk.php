<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/resources/slick/script.js","","",1);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.css");
});

//$500 off coupon form code sharpspring
add_action( 'gform_after_submission_7', 'post_to_third_party_7', 10, 2 );

function post_to_third_party_7( $entry, $form ) {
    $baseURI = 'https://app-3QNH93QVYY.marketingautomation.services/webforms/receivePostback/MzawMDE3MjM1BQA/';
    $endpoint = '171b13e6-8548-49be-9cd1-02749211aa25';
    $post_url = $baseURI . $endpoint;

    $field_id = 8; $field = GFFormsModel::get_field( $form, $field_id );
    $field_value_8 = is_object( $field ) ? $field->get_value_export( $entry ) : '';

    $body = array(
        'first' => rgar( $entry, '4' ),
        'last' => rgar( $entry, '5' ),
        'email' => rgar( $entry, '6' ),
        'phone' => rgar( $entry, '7' ),
        'terms conditions' => $field_value_8,
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

//Contact us form code sharpspring
add_action( 'gform_after_submission_6', 'post_to_third_party_6', 10, 2 );

function post_to_third_party_6( $entry, $form ) {
    $baseURI = 'https://app-3QNH93QVYY.marketingautomation.services/webforms/receivePostback/MzawMDE3MjM1BQA/';
    $endpoint = '09539c4f-0d0b-427c-bd8c-cb870529d94d';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '4' ),
        'last' => rgar( $entry, '5' ),
        'email' => rgar( $entry, '6' ),
        'phone' => rgar( $entry, '7' ),
        'questions comments' => rgar( $entry, '9' ),
        'terms conditions' => rgar( $entry, '8' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

//Schedule an appointme form code sharpspring
add_action( 'gform_after_submission_3', 'post_to_third_party_3', 10, 2 );

function post_to_third_party_3( $entry, $form ) {
    $baseURI = 'https://app-3QNH93QVYY.marketingautomation.services/webforms/receivePostback/MzawMDE3MjM1BQA/';
    $endpoint = 'be312028-2a22-4d4c-92c8-60f46d3cd668';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '1' ),
        'last' => rgar( $entry, '2' ),
        'phone' => rgar( $entry, '5' ),
        'email' => rgar( $entry, '4' ),
        'product choice' => rgar( $entry, '16' ),
        'preferred time' => rgar( $entry, '15' ),
        'preferred date' => rgar( $entry, '13' ),
        'terms conditions' => rgar( $entry, '12' ),
        'url' => rgar( $entry, '10' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}


// Classes
require_once 'classes/fl-class-child-theme.php';



add_action( 'after_setup_theme',     'FLChildTheme::setup' );
add_action( 'init',                  'FLChildTheme::init_woocommerce' );
add_action( 'wp_enqueue_scripts',    'FLChildTheme::enqueue_scripts', 999 );
add_action( 'widgets_init',          'FLChildTheme::widgets_init' );
add_action( 'wp_footer',             'FLChildTheme::go_to_top' );

// Theme Filters
add_filter( 'body_class',            'FLChildTheme::body_class' );
add_filter( 'excerpt_more',          'FLChildTheme::excerpt_more' );

// Actions
//add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 ); 

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
remove_action( 'wp_head', 'feed_links_extra', 3 );
/* 
if($_GET['keyword'] != '' && $_GET['brand'] !="")
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
  	    setcookie('keyword' , $_GET['keyword']);
	    setcookie('brand' , $_GET['brand']);
	    wp_redirect( $url[0] );
	    exit;
 } 
 else if($_GET['brand'] !="" && $_GET['keyword'] == '' )
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
        setcookie('brand' , $_GET['brand']);
        wp_redirect( $url[0] );
        exit;
 }
 else if($_GET['brand'] =="" && $_GET['keyword'] != '' )
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
        setcookie('keyword' , $_GET['keyword']);
        wp_redirect( $url[0] );
        exit;
 }
 
 
// shortcode to show H1 google keyword fields
function new_google_keyword() 
{
      
   if( $_COOKIE['keyword'] ==""  && $_COOKIE['brand'] == "")
   {
        return $google_keyword = '<h1 class="googlekeyword">Save up to $1000 on Shaw Flooring<h1>';
   }
   else
   {
       $keyword = $_COOKIE['keyword'];
	   $brand = $_COOKIE['brand'];
       return $google_keyword = '<h1 class="googlekeyword">Save up to $1000 on '.$brand.' '.$keyword.'<h1>';
   }
}
*/

function new_google_keyword() 
 {
	$keyword = $_GET['keyword'];
	$brand = $_GET['brand'];   
	if( $keyword ==""  && $brand == "")
    {	   
        return $google_keyword = '<h1 class="googlekeyword">SAVE UP TO $500 ON FLOORING*<h1>';
    }
    else
    {
         return $google_keyword = '<h1 class="googlekeyword">SAVE UP TO $500 ON '.$brand.' '.$keyword.'<h1>';
    }
 }
  add_shortcode('google_keyword_code', 'new_google_keyword');
  add_action('wp_head','cookie_gravityform_js');

  function cookie_gravityform_js()
  { // break out of php 
  ?>
  <script>
	 /* var brand_val ='<?php echo $_COOKIE['brand'];?>';
	  var keyword_val = '<?php echo $_COOKIE['keyword'];?>';  */
	  
	  var brand_val ='<?php echo $brand;?>';
	  var keyword_val = '<?php echo $keyword;?>'; 

      jQuery(document).ready(function($) {
      jQuery("#input_7_10").val(keyword_val);
      jQuery("#input_7_11").val(brand_val);
    });
  </script>
  <?php  
  /*   setcookie('keyword' , '',-3600); 
     setcookie('brand' , '',-3600); */
  
}

// Action to for styling H1 tag - google keyword fields
add_action('wp_head', 'add_css_head');
function add_css_head() {
    
   ?>
      <style>
          .googlekeyword {
             text-align:center;
             color: #fff;
             text-transform: capitalize;            
           }
      </style>  
   <?php    
}
function new_year_number() 
{
	return $new_year = date('Y');
}
add_shortcode('year_code_4', 'new_year_number');

function new_year_number_2() 
{
	return $new_year = date('y');
}
add_shortcode('year_code_2', 'new_year_number_2');

// Remove query string from static content
function _remove_script_version( $src ){ 
$parts = explode( '?', $src ); 	
return $parts[0]; 
} 
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 ); 
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );
?>