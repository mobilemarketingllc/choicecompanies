<?php

// Wrapper
if($show_author || $show_date || $comments) {
	
	echo '<div class="fl-post-meta fl-post-meta-top">';
	
	do_action( 'fl_post_top_meta_open' );
}
if(get_post_type() =='carpeting' || get_post_type() =='hardwood' || get_post_type() =='laminate' || get_post_type() =='luxury_vinyl_tile' || get_post_type() =='tile' ){
?>

<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <?php 
												
						if(get_post_type() =='carpeting') {
							$item = get_field('sku');
							$itemImage = explode("_", $item);	
							$imageNew= $itemImage[1] .'_'. $itemImage[0];
							$image = "http://shawfloors.scene7.com/is/image/ShawIndustries/".$imageNew."_SWATCH?fmt=pjpeg&fit=crop&wid=322&hei=322";
							
							$url ='https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_SWATCH?req=exists,text';
							$response = wp_remote_post($url, array('method' => 'GET'));		
							$ImageExist = $response["body"];	
							$exits = strpos($ImageExist,"catalogRecord.exists=0");		
							if ($exits !== false) {	 
								$image = "http://shawfloors.scene7.com/is/image/ShawIndustries/".$imageNew."_MAIN?fmt=pjpeg&fit=crop&wid=322&hei=322";
								$url ='https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_MAIN?req=exists,text';
								$response = wp_remote_post($url, array('method' => 'GET'));		
								$ImageExist = $response["body"];	
								$exits = strpos($ImageExist,"catalogRecord.exists=0");		
								if ($exits !== false) {	 
									$image = "http://shawfloors.scene7.com/is/image/ShawIndustriesRender/".$imageNew."_MAIN?fmt=pjpeg&fit=crop&wid=322&hei=322";
								}
							}
							
							$class = "";
						} else {
							
							$itemImage = get_field('swatch_image_link');
							$image= $itemImage . "?fmt=jpg&qlt=60&hei=322&wid=322&fit=crop,0";
							$class = "shadow";
						}
					?>
            <img class="<?php echo $class; ?>" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
                  

            </a> 
	<?php
	}
// Author
if ( $show_author ) {
	echo '<span class="fl-post-author">';
	printf( _x( 'By %s', 'Post meta info: author.', 'fl-automator' ), '<a href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '"><span>' . get_the_author_meta( 'display_name', get_the_author_meta( 'ID' ) ) . '</span></a>' );
	echo '</span>';
}

// Date
if($show_date) {

	if($show_author) {
		echo '<span class="fl-sep"> | </span>';
	}

	echo '<span class="fl-post-date">' . get_the_date() . '</span>';
}

// Comments
if($comments && $comment_count) {

	if($show_author || $show_date) {
		echo '<span class="fl-sep"> | </span>';
	}

	echo '<span class="fl-comments-popup-link">';
	comments_popup_link('0 <i class="fa fa-comment"></i>', '1 <i class="fa fa-comment"></i>', '% <i class="fa fa-comment"></i>');
	echo '</span>';
}

// Close Wrapper
if($show_author || $show_date || $comments) {
	
	do_action( 'fl_post_top_meta_close' );
	
	echo '</div>';
}

// Schema Meta
FLTheme::post_schema_meta();