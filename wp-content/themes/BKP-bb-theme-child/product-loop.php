<script
    type="text/javascript"
    async defer
    src="//assets.pinterest.com/js/pinit.js"
></script>
<div class="product-grid" itemscope itemtype="http://schema.org/CollectionPage">
    <div class="row">
        <?php 
            while ( have_posts() ): the_post(); 
                $postid = get_the_ID();
                $familykey = "collection";
                $familyvalue = get_post_meta($postid, 'collection', true);
                $flooringtype = get_post_type( get_the_ID() ); //'carpeting';
            
                $args = array(
                    'post_type'      => $flooringtype,
                    'posts_per_page' => -1,
                    'post_status'    => 'publish',
                    'meta_query'     => array(
                        array(
                            'key'     => $familykey,
                            'value'   => $familyvalue,
                            'compare' => '='
                        )
                    )
                );

                $the_query = new WP_Query( $args );
        ?>
                <div class="col-md-4 col-sm-4">
                    <div class="productName">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </div>
                    <div class="productImage">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <?php 
                            $itemImage = get_field('swatch_image_link');

                            if(strpos($itemImage , 's7.shawimg.com') !== false){
                                    if(strpos($itemImage , 'http') === false){ 
                                    $itemImage = "http://" . $itemImage;
                                    }	
                                $class = "";
                            }else{
                                    if(strpos($itemImage , 'http') === false){ 
                                        $itemImage = "https://" . $itemImage;
                                    }	
                                $class = "shadow";
                            }	
                            $image= "https://mobilem.liquifire.com/mobilem?source=url[".$itemImage ."]&scale=size[322]&sink";
            
                            ?>
                    <img class="<?php echo $class; ?>" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
                    </a>    
                    </div>    
                    <div class="productBrand"><?php echo get_field('brand');?></div>
                    <div class="productColor"><?php  echo $the_query->found_posts; ?> COLORS</div>
                    <div class="likeIcon">
                        <a href="#"><img src="/wp-content/uploads/2018/04/like-icon.jpg" /></a>
                    </div>
                    <div class="fl-module fl-module-button fl-node-5ad83d9354a3d" data-node="5ad83d9354a3d">
                        <div class="fl-module-content fl-node-content">
                            <div class="fl-button-wrap fl-button-width-auto fl-button-center" style="width: 76%;margin-top: 20px;display: inline-block;">
                                <a class="fl-button" role="button" href="/flooring-coupon/?keyword=<?php echo $_GET['keyword']; ?>&brand=<?php echo get_field('brand');?>" target="_self">
                                    <span class="fl-button-text" style="font-size:24px;">GET COUPON</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
        <?php 
            endwhile;   
        ?>
    </div>
</div>