<script
    type="text/javascript"
    async defer
    src="//assets.pinterest.com/js/pinit.js"
></script>
<?php


?>
<div class="product-grid" itemscope itemtype="http://schema.org/CollectionPage">
    <div class="row">
        <?php 
        
            while ( have_posts() ): the_post(); 
            $postid = get_the_ID();
            $familysku = get_post_meta($postid, 'style', true);
            $flooringtype = get_post_type( get_the_ID() ); //'carpeting';
            if( get_field('manufacturer') == 'COREtec' ){
						$value = get_post_meta($post->ID, 'color', true);
						$key =  "color";
					} else {

						$value = get_post_meta($post->ID, 'style', true);
						$key =  "style";
					}
            $args = array(
                'post_type'      => $flooringtype,
                'posts_per_page' => -1,
                'post_status'    => 'publish',
                'meta_query'     => array(
                    array(
                        'key'     => $key,
                        'value'   => $value,
                        'compare' => '='
                    )
                )
            );

            $the_query = new WP_Query( $args );
        ?>
        <div class="col-md-4 col-sm-4">
            <div class="productName">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </div>
<!--              <div class="productColor"><?php  echo $the_query->found_posts; ?> COLORS</div>  -->
            <div class="productImage">
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <?php 
												
						if(get_post_type() =='carpeting') {
							$item = get_field('sku');
							$itemImage = explode("_", $item);	
							$imageNew= $itemImage[1] .'_'. $itemImage[0];
							$image = "http://shawfloors.scene7.com/is/image/ShawIndustries/".$imageNew."_SWATCH?fmt=pjpeg&fit=crop&wid=322&hei=322";
							
							$url ='https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_SWATCH?req=exists,text';
							$response = wp_remote_post($url, array('method' => 'GET'));		
							$ImageExist = $response["body"];	
							$exits = strpos($ImageExist,"catalogRecord.exists=0");		
							if ($exits !== false) {	 
								$image = "http://shawfloors.scene7.com/is/image/ShawIndustries/".$imageNew."_MAIN?fmt=pjpeg&fit=crop&wid=322&hei=322";
								$url ='https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_MAIN?req=exists,text';
								$response = wp_remote_post($url, array('method' => 'GET'));		
								$ImageExist = $response["body"];	
								$exits = strpos($ImageExist,"catalogRecord.exists=0");		
								if ($exits !== false) {	 
									$image = "http://shawfloors.scene7.com/is/image/ShawIndustriesRender/".$imageNew."_MAIN?fmt=pjpeg&fit=crop&wid=322&hei=322";
								}
							}
							
							$class = "";
						} else {
							if(get_field('swatch_image_link') != ""){
			
								$itemImage = get_field('swatch_image_link');
								$image= $itemImage . "?fmt=jpg&qlt=60&hei=322&wid=322&fit=crop,0";
								$class = "shadow";
							}else{
								$https= "https://placehold.it/322x322/F2F2F2/000000?text=Coming+Soon";
							}	
						}
					?>
            <img class="<?php echo $class; ?>" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
                  

            </a>    
            </div>    
            <div class="productBrand"><?php echo get_field('brand');?></div>
			<div class="productColor"><?php  echo $the_query->found_posts; ?> COLORS</div>
            <div class="likeIcon">
                <a href="#"><img src="/wp-content/uploads/2018/04/like-icon.jpg" /></a>
            </div>
            <div class="fl-module fl-module-button fl-node-5ad83d9354a3d" data-node="5ad83d9354a3d">
                <div class="fl-module-content fl-node-content">
                    <div class="fl-button-wrap fl-button-width-auto fl-button-center" style="width: 76%;margin-top: 20px;display: inline-block;">
                        <a class="fl-button" role="button" href="/flooring-coupon/?keyword=<?php echo $_GET['keyword']; ?>&brand=<?php echo get_field('brand');?>" target="_self">
                            <span class="fl-button-text" style="font-size:24px;">GET COUPON</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <?php 
            endwhile;   
        ?>
    </div>
</div>