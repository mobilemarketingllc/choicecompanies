<?php do_action( 'fl_content_close' ); ?>

	</div><!-- .fl-page-content -->
	<?php

	do_action( 'fl_after_content' );

	if ( FLTheme::has_footer() ) :

		?>
	<footer class="fl-page-footer-wrap"<?php FLTheme::print_schema( ' itemscope="itemscope" itemtype="https://schema.org/WPFooter"' ); ?>>
		<?php

		do_action( 'fl_footer_wrap_open' );
		do_action( 'fl_before_footer_widgets' );

		FLTheme::footer_widgets();

		do_action( 'fl_after_footer_widgets' );
		do_action( 'fl_before_footer' );

		FLTheme::footer();

		do_action( 'fl_after_footer' );
		do_action( 'fl_footer_wrap_close' );

		?>
	</footer>
	<?php endif; ?>
	<?php do_action( 'fl_page_close' ); ?>
</div><!-- .fl-page -->
<?php

wp_footer();

do_action( 'fl_body_close' );

FLTheme::footer_code();

?>
<?php if($_GET['get']=='lvt'){ ?>
<script>
 jQuery(document).ready(function(){
    jQuery( ".fl-tabs-labels .fl-tabs-label:nth-child(5)" ).addClass( "fl-tab-active" );
	jQuery( ".fl-tabs-labels .fl-tabs-label:nth-child(1)" ).removeClass( "fl-tab-active" );	
	jQuery( ".fl-tabs-panels .fl-tabs-panel:nth-child(5) .fl-tabs-panel-content" ).addClass( "fl-tab-active" );
	jQuery( ".fl-tabs-panels .fl-tabs-panel:nth-child(1) .fl-tabs-panel-content" ).removeClass( "fl-tab-active" );
	
  });
</script>
<?php } else if($_GET['get']=='laminate'){ ?>
<script>
 jQuery(document).ready(function(){
    jQuery( ".fl-tabs-labels .fl-tabs-label:nth-child(3)" ).addClass( "fl-tab-active" );
	jQuery( ".fl-tabs-labels .fl-tabs-label:nth-child(1)" ).removeClass( "fl-tab-active" );	
	jQuery( ".fl-tabs-panels .fl-tabs-panel:nth-child(3) .fl-tabs-panel-content" ).addClass( "fl-tab-active" );
	jQuery( ".fl-tabs-panels .fl-tabs-panel:nth-child(1) .fl-tabs-panel-content" ).removeClass( "fl-tab-active" ); 
  });
</script>
<?php } else if($_GET['get']=='hardwood'){ ?>
<script>
 jQuery(document).ready(function(){
    jQuery( ".fl-tabs-labels .fl-tabs-label:nth-child(2)" ).addClass( "fl-tab-active" );
	jQuery( ".fl-tabs-labels .fl-tabs-label:nth-child(1)" ).removeClass( "fl-tab-active" );	
	jQuery( ".fl-tabs-panels .fl-tabs-panel:nth-child(2) .fl-tabs-panel-content" ).addClass( "fl-tab-active" );
	jQuery( ".fl-tabs-panels .fl-tabs-panel:nth-child(1) .fl-tabs-panel-content" ).removeClass( "fl-tab-active" );
  });
</script>
<?php } else if($_GET['get']=='tile'){ ?>
<script>
 jQuery(document).ready(function(){
    jQuery( ".fl-tabs-labels .fl-tabs-label:nth-child(4)" ).addClass( "fl-tab-active" );
	jQuery( ".fl-tabs-labels .fl-tabs-label:nth-child(1)" ).removeClass( "fl-tab-active" );	
	jQuery( ".fl-tabs-panels .fl-tabs-panel:nth-child(4) .fl-tabs-panel-content" ).addClass( "fl-tab-active" );
	jQuery( ".fl-tabs-panels .fl-tabs-panel:nth-child(1) .fl-tabs-panel-content" ).removeClass( "fl-tab-active" ); 
  });
</script>
<?php } ?>
<script type="text/javascript">
// if(jQuery(window).width()=>768){
// 	jQuery('.navbar-nav .menu-item-has-children').mouseover(function(){
// 		jQuery('header.fl-page-header-primary').after('<div class="navHover" />');
// 	});

// 	jQuery('.navbar-nav .menu-item-has-children').mouseout(function(){
// 		jQuery('.navHover').remove();
// 	});
// })
</script>
<script type="text/javascript">
    var _ss = _ss || [];
    _ss.push(['_setDomain', 'https://koi-3QNH93QVYY.marketingautomation.services/net']);
    _ss.push(['_setAccount', 'KOI-42L6C70DCY']);
    _ss.push(['_trackPageView']);
(function() {
    var ss = document.createElement('script');
    ss.type = 'text/javascript'; ss.async = true;
    ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-3QNH93QVYY.marketingautomation.services/client/ss.js?ver=1.1.1';
    var scr = document.getElementsByTagName('script')[0];
    scr.parentNode.insertBefore(ss, scr);
})();
</script>
</body>
</html>
